import os
from flask import Flask, render_template, request, jsonify, send_from_directory
from subsai import SubsAI

app = Flask(__name__)

# Define the directory paths for uploaded files and the export folder
UPLOAD_FOLDER = os.path.join(os.getcwd(), "uploads")
EXPORT_FOLDER = os.path.join(os.getcwd(), "export")

app.config["UPLOAD_FOLDER"] = UPLOAD_FOLDER
app.config["EXPORT_FOLDER"] = EXPORT_FOLDER


@app.route("/upload", methods=["POST"])
def upload_file():
    if "file" not in request.files:
        return "No file part"

    file = request.files["file"]

    if file.filename == "":
        return "No selected file"

    if not os.path.isfile(os.path.join(app.config["UPLOAD_FOLDER"], file.filename)):
        # Now you can save the file to the specified directory
        file.save(os.path.join(app.config["UPLOAD_FOLDER"], file.filename))

    return "File uploaded successfully"


@app.route("/")
def home():
    return render_template("index.html")


@app.route("/generate-subtitles", methods=["POST"])
def generate_subtitles():
    file_name = request.json["file_name"]
    del_gen_file = bool(request.json["del_gen_file"])
    subs_path = f"{get_export_folder()}/{file_name}.srt"

    if not os.path.isfile(subs_path):
        subs_ai = SubsAI()
        model = subs_ai.create_model("openai/whisper", {"model_type": "base"})
        print(f"transcribing {file_name}")
        subs = subs_ai.transcribe(f'{app.config["UPLOAD_FOLDER"]}/{file_name}', model)

        subs.save(subs_path)

    with open(subs_path, "r", encoding="utf-8") as subs_file:
        subtitles = subs_file.read()

    print("done")
    if del_gen_file:
        os.remove(subs_path)
    return jsonify({"subtitles": subtitles})


@app.route("/read-file", methods=["POST"])
def read_file():
    file_name = request.json["file_name"]

    with open(file_name, "r", encoding="utf-8") as subs_file:
        file_content = subs_file.read()

    return jsonify({"file_content": file_content})


@app.route("/favicon.ico")
def favicon():
    return send_from_directory(
        os.path.join(app.root_path, "static"),
        "favicon.ico",
        mimetype="image/vnd.microsoft.icon",
    )


def get_export_folder():
    export_folder = app.config["EXPORT_FOLDER"]
    if not os.path.exists(export_folder):
        os.makedirs(export_folder)
    return export_folder


if __name__ == "__main__":
    print("Server start...")
    app.run(host="127.0.0.1", port=5000, debug=True)
    print("Server online! server addr : 127.0.0.1:5000")
