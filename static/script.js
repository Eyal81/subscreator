const fileInput = document.getElementById('file');
const fileLabel = document.getElementById('file-label');
const videoPathLabel = document.getElementById('video-path-label');
const videoContainer = document.getElementById('video-container');
const videoTime = document.getElementById('video-time');
const videoSubs = document.getElementById('video-subs');
const subtitlesTableBody = document.getElementById('subtitles-table-body');
const startTimeInput = document.getElementById('start-time-input');
const endTimeInput = document.getElementById('end-time-input');
const subtitleTextInput = document.getElementById('subtitle-text');
const addSubtitleBtn = document.getElementById('add-subtitle-btn');
const video = document.createElement('video');


let subtitlesData = [];

fileInput.addEventListener('change', handleFileInputChange);

function handleFileInputChange() {
    const file = fileInput.files[0];
    fileLabel.textContent = (file ? file.name : 'No file selected') + ';';

    if (file) {
        loadVideo(file);
        var formData = new FormData();
        formData.append('file', file);
        $.ajax({
            url: '/upload',
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            success: function (response) {
                console.log(response);
            },
            error: function (xhr, status, error) {
                console.error(error);
            }
        });
    }
}

function loadVideo(file) {
    const videoURL = URL.createObjectURL(file);
    video.src = videoURL;
    video.controls = true;
    video.autoplay = false;

    videoContainer.innerHTML = '';
    videoContainer.appendChild(video);

    video.addEventListener('timeupdate', handleVideoTimeUpdate);
}



function handleVideoTimeUpdate() {

    const currentTime = video.currentTime * 1000;


    // Find the current subtitle based on the current time
    const currentSubtitle = subtitlesData.find((subtitle) => {
        const startTime = parseTime(subtitle.start);
        const endTime = parseTime(subtitle.end);
        return startTime <= currentTime && currentTime <= endTime;
    });

    // Update the current subtitle text
    if (currentSubtitle) {
        console.log(currentSubtitle);
        videoSubs.textContent = currentSubtitle.text;
    } else {
        videoSubs.textContent = '';

    }


    const duration = video.duration * 1000;
    const formattedTime = formatTime(currentTime) + ' / ' + formatTime(duration);
    videoTime.textContent = 'Time: ' + formattedTime;

}

function formatTime(time) {
    const milliseconds = Math.floor(time % 1000);
    const seconds = Math.floor((time / 1000) % 60);
    const minutes = Math.floor((time / (1000 * 60)) % 60);
    const hours = Math.floor((time / (1000 * 60 * 60)) % 24);

    return (
        hours.toString().padStart(2, '0') +
        ':' +
        minutes.toString().padStart(2, '0') +
        ':' +
        seconds.toString().padStart(2, '0') +
        '.' +
        milliseconds.toString().padStart(3, '0')
    );
}

addSubtitleBtn.addEventListener('click', handleAddSubtitle);

function handleAddSubtitle() {
    const startTime = parseTime(startTimeInput.value);
    const endTime = parseTime(endTimeInput.value);
    const subtitle = subtitleTextInput.value;

    if (
        !isNaN(startTime) &&
        !isNaN(endTime) &&
        subtitle !== '' &&
        startTime < endTime
    ) {
        const subtitleObj = {
            start: startTimeInput.value,
            end: endTimeInput.value,
            text: subtitle
        };
        subtitlesData.push(subtitleObj);
        clearSubtitleForm();
        sortTable();
    } else {
        alert(
            'Please enter valid start and end times, and ensure that the start time is less than the end time.'
        );
    }
}



function parseTime(time) {
    const parts = time.split(':');
    const hours = parseInt(parts[0]) || 0;
    const minutes = parseInt(parts[1]) || 0;
    const seconds = parseFloat(parts[2]) || 0;
    return (hours * 60 * 60 * 1000) + (minutes * 60 * 1000) +
        (seconds * 1000);
}



function addSubtitleRow(subtitle) {
    const row = document.createElement('tr');
    const startCell = document.createElement('td');
    const endCell = document.createElement('td');
    const subtitleCell = document.createElement('td');
    const actionCell = document.createElement('td');
    const playCell = document.createElement('td');
    const editButton = document.createElement('button');
    const playButton = document.createElement('button');

    console.log(subtitle);
    startCell.textContent = subtitle.start;
    endCell.textContent = subtitle.end;
    subtitleCell.textContent = subtitle.text;

    subtitleCell.setAttribute('dir', 'rtl');

    playButton.innerHTML = '<img src="/static/play_icon.png" alt="Play">';
    playButton.addEventListener('click', () => {
        const selectedOption = document.getElementById('play-options').value;

        const startTime = parseTime(subtitle.start);
        const endTime = parseTime(subtitle.end);

        if (video && !isNaN(startTime)) {
            video.currentTime = startTime / 1000;
            video.play();
            switch (selectedOption) {
                case "skip-pause":
                    video.addEventListener('timeupdate', function skipPause() {
                        if (video.currentTime >= endTime / 1000) {
                            video.pause();
                            video.removeEventListener('timeupdate', skipPause);
                        }
                    });
                    break;
                case "skip-loop":
                    const loopCountInput = document.getElementById('loop-input');
                    video.addEventListener('timeupdate', function skipLoop() {
                        let loopCount = parseInt(loopCountInput.value, 0);
                        if (loopCount > 0 && video.currentTime >= endTime / 1000) {
                            video.currentTime = startTime / 1000;
                            loopCountInput.value = --loopCount;
                            if (loopCount === 0) {
                                video.removeEventListener('timeupdate', skipLoop);
                                loopCountInput.value = 1;

                            }
                        }
                    });
                    break;
                default:
                    break;
            }
        }
    });
    playCell.appendChild(playButton);

    editButton.innerHTML = '<img src="/static/edit_icon.png" alt="Edit">';
    editButton.addEventListener('click', () => {
        makeRowEditable(row);
    });
    actionCell.appendChild(editButton);


    row.appendChild(startCell);
    row.appendChild(endCell);
    row.appendChild(subtitleCell);
    row.appendChild(playCell);
    row.appendChild(actionCell);

    subtitlesTableBody.appendChild(row);
}

function clearSubtitleForm() {
    startTimeInput.value = subtitlesData[subtitlesData.length - 1].end || '00:00:00.000';
    endTimeInput.value = formatTime(parseTime(startTimeInput.value) + 1519);
    subtitleTextInput.value = '';
}

function sortTable() {
    subtitlesData.sort((a, b) => parseTime(a.start) - parseTime(b.start));
    subtitlesTableBody.innerHTML = '';
    subtitlesData.forEach(subtitle => {
        addSubtitleRow(subtitle);
    });
}


function makeRowEditable(row) {

    const actionCell = row.querySelector('td:last-child');

    const saveButton = document.createElement('button');
    const removeButton = document.createElement('button');

    const defaultRowHeight = row.style.height;
    row.style.height = '120px';

    saveButton.innerHTML = '<img src="/static/save_icon.png" alt="Save">';
    saveButton.addEventListener('click', () => {
        handleSaveSubtitleRow(row);

        row.style.height = defaultRowHeight;

        actionCell.innerHTML = '';
        const editButton = document.createElement('button');
        editButton.innerHTML = '<img src="/static/edit_icon.png" alt="Edit">';
        editButton.addEventListener('click', () => {
            makeRowEditable(row);
        });
        actionCell.appendChild(editButton);
    });

    removeButton.innerHTML = '<img src="/static/remove_icon.png"  alt="Remove">';
    removeButton.style.marginTop = "30px";
    removeButton.addEventListener('click', () => {
        removeSubtitleRow(row);
    });


    actionCell.innerHTML = '';
    actionCell.appendChild(saveButton);
    actionCell.appendChild(removeButton);

    const startCell = row.children[0];
    const endCell = row.children[1];
    const subtitleCell = row.children[2];

    startCell.innerHTML = `<input type="text" class="edit-input time-input" style="width: 120px;" value="${startCell.textContent}" id="start-time-input">`;
    endCell.innerHTML = `<input type="text" class="edit-input time-input" style="width: 120px;" value="${endCell.textContent}" id="end-time-input">`;
    subtitleCell.innerHTML = `<textarea class="edit-textarea" spellcheck="true" style="width: 100%; height: 100%;" dir="rtl">${subtitleCell.textContent}</textarea>`;

    $('#start-time-input').mask('99:99:99.999');
    $('#end-time-input').mask('99:99:99.999');
}



function removeSubtitleRow(row) {
    const index = Array.from(subtitlesTableBody.children).indexOf(row);
    if (index !== -1) {
        subtitlesTableBody.removeChild(row);
        subtitlesData.splice(index, 1);
        clearSubtitleForm();
    }
}

function handleSaveSubtitleRow(row) {

    const rowIndex = Array.from(subtitlesTableBody.children).indexOf(row);
    if (rowIndex !== -1) {
        const startCell = row.children[0];
        const endCell = row.children[1];
        const subtitleCell = row.children[2];

        const startTimeInput = startCell.querySelector('input');
        const endTimeInput = endCell.querySelector('input');
        const subtitleTextarea = subtitleCell.querySelector('textarea');

        if (startTimeInput && endTimeInput) {
            const startTime = parseTime(startTimeInput.value);
            const endTime = parseTime(endTimeInput.value);
            const subtitle = subtitleTextarea.value;

            if (
                !isNaN(startTime) &&
                !isNaN(endTime) &&
                subtitle !== '' &&
                startTime < endTime
            ) {
                const subtitleObj = subtitlesData[rowIndex];
                subtitleObj.start = startTimeInput.value;
                subtitleObj.end = endTimeInput.value;
                subtitleObj.text = subtitle;
                sortTable();
            } else {
                alert(
                    'Please enter valid start and end times, and ensure that the start time is less than the end time.'
                );
            }
        }
        else {
            alert(
                'Please enter valid start and end times, and ensure that the start time is less than the end time.'
            );
        }

    }
}




$(document).ready(function () {
    $('.time-input').mask('99:99:99.999');
    startTimeInput.value = '00:00:00.000';
    endTimeInput.value = '00:00:01.519';
});



const timeInputs = document.querySelectorAll('.edit-input.time-input');
const shiftStepInput = document.getElementById('shift-step-input');

timeInputs.forEach((input) => {
    input.addEventListener('keydown', (event) => {
        const key = event.key;
        const increment = Number(shiftStepInput.value) || 1;

        if (key === 'ArrowUp' || key === 'ArrowDown') {
            event.preventDefault();

            const currentTime = parseTime(input.value);
            let updatedTime;

            if (key === 'ArrowUp') {
                updatedTime = currentTime + increment;
            } else {
                updatedTime = currentTime - increment;
                if (updatedTime < 0) {
                    updatedTime = 0; // Prevent going below zero
                }
            }

            const formattedTime = formatTime(updatedTime);
            input.value = formattedTime;
        }
    });
});

const genSubsButton = document.getElementById('subtitles-gen-button');


genSubsButton.addEventListener("click", function () {
    const file = fileInput.files[0];
    const delGenFile = $('#del-gen-file').is(":checked")
    if (file) {

        Swal.fire('Please wait')
        Swal.showLoading()


        $.ajax({
            type: "POST",
            url: "/generate-subtitles",
            data: JSON.stringify({ file_name: file.name, del_gen_file: delGenFile }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                Swal.close();
                // Update the table with the generated subtitles
                const subtitles = data.subtitles;
                updateTable(subtitles);
            }
        });
    }
    else {
        alert('No file selected!');
    }
});


function updateTable(subs) {
    // Clear existing table rows
    subtitlesTableBody.innerHTML = '';
    subtitlesData = [];

    // Parse the SRT data using parseSrt function
    const subtitles = parseSrt(subs);


    // Iterate over the subtitles and add rows to the table
    subtitles.forEach((subtitle) => {
        const subtitleObj = {
            start: subtitle.start,
            end: subtitle.end,
            text: subtitle.text
        };
        subtitlesData.push(subtitleObj);
        addSubtitleRow(subtitle);
    });
}


// Parse SRT data into an array of objects
function parseSrt(srtData) {
    const subs = [];

    const regex = /(\d+)\n([\d:,\.]+) --> ([\d:,\.]+)\n([\s\S]*?)(?=\n\n|\n$|$)/g;
    let match;

    while ((match = regex.exec(srtData))) {
        const id = parseInt(match[1]);
        const start = match[2].replace(',', '.');
        const end = match[3].replace(',', '.');
        const text = match[4].trim();

        subs.push({ id, start, end, text });
    }
    return subs;
}




const importSubtitlesBtn = document.getElementById('import-subtitles-btn');
const importSubtitlesInput = document.getElementById('import-subtitles-input');

importSubtitlesBtn.addEventListener('click', function () {
    importSubtitlesInput.click();
});

importSubtitlesInput.addEventListener('change', handleImportSubtitles);


function handleImportSubtitles() {
    const file = importSubtitlesInput.files[0];
    if (file) {

        $.ajax({
            type: "POST",
            url: "/read-file",
            data: JSON.stringify({ file_name: file.name }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                Swal.close();
                // Update the table with the generated subtitles
                const subtitles = data.file_content;
                updateTable(subtitles);
            }
        });

    }
}


const exportSubtitlesBtn = document.getElementById('export-subtitles-btn');
exportSubtitlesBtn.addEventListener('click', handleExportSubtitles);

function handleExportSubtitles() {
    // Create an SRT subtitle string from the subtitlesData array
    const srtContent = createSRTContent(subtitlesData);

    // Prompt the user to select the export file path
    const saveAsDialog = document.createElement('a');
    saveAsDialog.href = 'data:text/plain;charset=utf-8,' + encodeURIComponent(srtContent);
    saveAsDialog.download = 'subtitles.srt';
    saveAsDialog.style.display = 'none';

    // Trigger the download
    document.body.appendChild(saveAsDialog);
    saveAsDialog.click();
    document.body.removeChild(saveAsDialog);
}


function createSRTContent(subtitles) {
    let srtContent = '';

    subtitles.forEach((subtitle, index) => {
        const id = index + 1;
        const startTime = subtitle.start;
        const endTime = subtitle.end;
        const text = subtitle.text;

        srtContent += `${id}\n${startTime} --> ${endTime}\n${text}\n\n`;
    });

    return srtContent.trim();
}


